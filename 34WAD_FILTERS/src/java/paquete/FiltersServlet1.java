
package paquete;

import java.io.IOException;
import java.util.Date;
import java.util.logging.LogRecord;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

public class FiltersServlet1 implements Filter{

    @Override
    public void init(FilterConfig config) throws ServletException {
          String parametroprueba = config.getInitParameter("parametroprueba");
        System.out.println("Parametro de prueba: " + parametroprueba);
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest requesthttp = (HttpServletRequest) request;
        String ip = requesthttp.getRemoteAddr();
        System.out.println("IP:"+ip + ", TIEMPO:"+ new Date().toString());
        chain.doFilter(request, response);
    }

    @Override
    public void destroy() {
    }

 
    
    
}
