package paquete;

import com.opensymphony.xwork2.ActionSupport;
import java.util.Map;
import org.apache.struts2.interceptor.SessionAware;

public class Accion extends ActionSupport implements SessionAware
{
private String username; 
private String password;
private Map<String, Object> sessionMap;
     
    
    @Override
    public void setSession(Map<String, Object> sessionMap) 
    {//A
    this.sessionMap = sessionMap;
    }    
    
    public String getUsername() 
    {
    return username;
    }
 
    public void setUsername(String username) 
    {
    this.username = username;
    }
    public String getPassword()
    {
        return password;
    }
   public void setPassword(String pass){
   this.password=pass;
   }
	
@Override
    public String execute() 
    {
        LoginBean lb = new LoginBean();
          if(lb.validateUser(username, password)){
          sessionMap.put("username", username);
             return "exitoso";
          }
     return "fall";
    }    
}
