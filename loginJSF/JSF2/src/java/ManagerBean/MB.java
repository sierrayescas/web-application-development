package ManagerBean;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

@ManagedBean
@RequestScoped
public class MB 
{
private String id;
private String password;
private final HttpServletRequest request;
private final FacesContext fc;
private FacesMessage fm;

    public MB() 
    {
        fc= FacesContext.getCurrentInstance();
        request = (HttpServletRequest)fc.getExternalContext().getRequest();
    }
    
    public String validate()
    {
        if(id.compareTo("ruben")==0)
        {
            request.getSession().setAttribute("sesionusuario", id);
            fm = new FacesMessage(FacesMessage.SEVERITY_INFO,"Correcto",null);
            fc.addMessage(null, fm);
            return "salida0";
        }
        else
        {
            fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,"Incorrecto",null);
            fc.addMessage(null, fm);
            return "index";        
        }
    }
    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
}
