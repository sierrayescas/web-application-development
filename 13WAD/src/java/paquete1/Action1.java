/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package paquete1;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author Eduardo
 */
public class Action1 extends org.apache.struts.action.Action {

    /* forward name="success" path="" */
    private static final String SUCCESS = "success";
    private static final String FAIL = "fail";

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        ActionForm1 forma = (ActionForm1) form;
        loginBean login = new loginBean();
        if (login.validateUser(forma.getId(), forma.getPassword())) {
            HttpSession session = request.getSession();
            session.setAttribute("id", forma.getId());
            return mapping.findForward(SUCCESS);
        } else {
            return mapping.findForward(FAIL);
        }
    }
}
