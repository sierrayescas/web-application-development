/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package paquete;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.PrintWriter;
import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Eduardo
 */
public class Servlet11 extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
        @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
          
    response.setContentType("image/jpeg");
    try  
            {  
                //Create an image 200 x 200  
                BufferedImage bufferedImage = new BufferedImage(200, 200,   
                             BufferedImage.TYPE_INT_RGB);  
      
                //Draw an oval  
                Graphics g = bufferedImage.getGraphics();  
                g.setColor(Color.blue);  
                g.fillOval(0, 0, 199,199);  
      
                //Free graphic resources  
                g.dispose();  
      
                //Write the image as a jpg 
                ImageIO.write(bufferedImage, "jpg", response.getOutputStream());  
            }  
            catch (IOException e)  
            {  
             e.printStackTrace();
            }  
    }
}
