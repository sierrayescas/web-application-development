package actionsupportpackage;

import com.opensymphony.xwork2.ActionSupport;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.*;
import org.apache.struts2.ServletActionContext;

public class LoginActionSupport extends ActionSupport 
{
 String userName,password;
 
 public String execute() throws Exception 
 {
        LoginBean lb = new LoginBean();
        
        // check to see if this user/password combination are valid
        if(lb.validateUser(userName,password))
        {
            HttpServletRequest request = ServletActionContext.getRequest();
            HttpSession sesion = request.getSession();
            sesion.setAttribute("ID", userName);
        return SUCCESS; 
        }
        else	// username/password not validated
        {
        addActionError("User Name or password does not exist");
        return INPUT;
        }  
 }
 public String getPassword() {
 return password;
 }
 public void setPassword(String password) {
 this.password = password;
 }
 public String getUserName() {
 return userName;
 }
 public void setUserName(String userName) {
 this.userName = userName;
 }
}