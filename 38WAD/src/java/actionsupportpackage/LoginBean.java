package actionsupportpackage;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
//rperedo rpv
class LoginBean {

    private HashMap validUsers = new HashMap();

    /**
     * Constructor for LoginBean Initializes the list of usernames/passwords
     */
    public LoginBean() {
        try {
            //          validUsers.put("ruben","rpv");
            //   validUsers.put("administrator","admin");
            ResultSet rs = null;
            Class.forName("com.mysql.cj.jdbc.Driver");
            Connection db = DriverManager.getConnection("jdbc:mysql://localhost/Users?autoReconnect=true&useSSL=false&useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC", "root", "root");
            Statement s = db.createStatement();
            rs = s.executeQuery("SELECT * FROM user_account");
            while(rs.next()){
                validUsers.put(rs.getString("id"),rs.getString("password"));
            }
            
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * determine if the username/password combination are present in the
     * validUsers repository.
     *
     * @param userName
     * @param password
     * @return boolean true if valid, false otherwise
     */
    public boolean validateUser(String userName, String password) {
        if (validUsers.containsKey(userName)) {
            String thePassword = (String) validUsers.get(userName);
            if (thePassword.equals(password)) {
                return true;
            }
        }
        return false;
    }

}
