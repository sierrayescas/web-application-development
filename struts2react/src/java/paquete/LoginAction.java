package paquete;

import com.opensymphony.xwork2.ActionSupport;
import java.io.File;
import java.io.PrintWriter;
import java.util.List;
import org.apache.struts2.ServletActionContext;
import org.jdom2.input.SAXBuilder;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;

public class LoginAction extends ActionSupport 
{
 String username,password;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
 
 @Override
 public String execute() throws Exception 
 {       
        String ruta = ServletActionContext.getServletContext().getRealPath("/");
        try
        {
        SAXBuilder builder = new SAXBuilder();
        File archivoXML = new File(ruta+"\\archivoXML.xml");
        Document documento=builder.build(archivoXML);
        Element raiz = documento.getRootElement();
        List lista=raiz.getChildren("usuario");
            for(int i=0;i<lista.size();i++)
            {
             Element elemento = (Element)lista.get(i);
             String usernamexml=elemento.getAttributeValue("username");
             String passwordxml=elemento.getAttributeValue("password");             
             if(username.compareTo(usernamexml)==0&&password.compareTo(passwordxml)==0)
             return SUCCESS;
            }
        }
        catch(JDOMException e)
        {
        e.printStackTrace();
        }   
        return ERROR;        
 }
 
}