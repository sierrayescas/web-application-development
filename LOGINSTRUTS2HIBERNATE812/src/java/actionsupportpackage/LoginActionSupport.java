package actionsupportpackage;

import com.opensymphony.xwork2.ActionSupport;
import entity.Login;
import entity.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class LoginActionSupport extends ActionSupport {
 String userName,password;
 Session hibernateSession;
 Login login;
 public String execute() throws Exception {
 hibernateSession=HibernateUtil.getSessionFactory().openSession(); 
 Transaction t1=hibernateSession.beginTransaction();
 login=(Login) hibernateSession.createQuery("from Login where userName='"+userName+"'AND password='"+password+"'").uniqueResult();
 t1.commit();
 
 if(userName!=null && password!=null &&(!userName.equals(""))&&(!password.equals("")))
 {
  if(login!=null)
  return SUCCESS; 
 }

 addActionError("User Name or password does not exist");
 return INPUT;

 }
 public String getPassword() {
 return password;
 }
 public void setPassword(String password) {
 this.password = password;
 }
 public String getUserName() {
 return userName;
 }
 public void setUserName(String userName) {
 this.userName = userName;
 }
}