/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package paquete;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.PrintWriter;
import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Eduardo
 */
public class Servlet11 extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int xNosePoints[] = {250, 230, 270};
        int yNosePoints[] = {270, 250, 250};
        response.setContentType("image/jpeg");
        try {
            //Create an image 200 x 200  
            BufferedImage bufferedImage = new BufferedImage(500, 500,
                    BufferedImage.TYPE_INT_RGB);

            //Draw an oval  
            Graphics g = bufferedImage.getGraphics();
            g.setColor(Color.getHSBColor(99, 65, 15));
            g.fillOval(130, 100, 100, 150);
            g.fillOval(270, 100, 100, 150);
            g.setColor(Color.RED);
            g.fillOval(140, 120, 70, 130);
            g.fillOval(290, 120, 70, 130);
            g.setColor(Color.getHSBColor(99, 65, 15));
            g.fillOval(175, 150, 150, 200);
            g.setColor(Color.DARK_GRAY);
            g.fillOval(190, 200, 20, 20);
            g.fillOval(290, 200, 20, 20);
            g.fillPolygon(xNosePoints,yNosePoints, 3);
                g.setColor(Color.white);
            g.fillOval(195, 205, 5, 5);
            g.fillOval(295, 205, 5, 5);
            g.fillOval(235, 250, 10, 5);
            g.setColor(Color.yellow);
            //Free graphic resources  
            g.dispose();

            //Write the image as a jpg 
            ImageIO.write(bufferedImage, "jpg", response.getOutputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
